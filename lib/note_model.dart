// the note model
class Note {
  int _id; // the id generated automatically
  String _content;
  String _date;

  Note(this._date, this._content); // without _id

  // named constructor tack the id as a parameter
  Note.withId(this._id, this._date, this._content);

  // getter and setter methods
  int get id => _id;

  String get date => _date;

  set date(String value) {
    _date = value;
  }

  String get content => _content;

  set content(String value) {
    _content = value;
  }

  // convert note to map
  Map<String, dynamic> noteToMap() {
    var map = Map<String, dynamic>();

    if (id != null) {
      map['id'] = this._id;
    }
    map['content'] = this._content;
    map['date'] = this._date;
    return map;
  }

  // extract values from map to note
  Note.fromMapObject(Map<String, dynamic> map) {
    this._id = map['id'];
    this._content = map['content'];
    this._date = map['date'];
  }
}

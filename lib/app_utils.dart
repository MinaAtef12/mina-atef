import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


import 'database_helper.dart';
import 'note_model.dart';

String appName = 'Voice diary app';

void showToast(String msg, {Color color}) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    backgroundColor: color ?? Colors.red,
    textColor: Colors.white,
    fontSize: 16.0,
  );
}

Future<int> getNotesCount(DatabaseHelper databaseHelper) async {
  return await databaseHelper.getNotesCount();
}

Future<List<Note>> getNotes(DatabaseHelper databaseHelper) async {
  return await databaseHelper.getAllNotes();
}

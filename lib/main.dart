import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

import 'app_utils.dart';
import 'database_helper.dart';
import 'note_model.dart';
import 'recording_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DatabaseHelper _databaseHelper = DatabaseHelper();
  int _notesCount = 0;
  List<Note> _notesList;

  @override
  void initState() {
    super.initState();

    _initNotes();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, designSize: size, allowFontScaling: true);
    return Scaffold(
      bottomNavigationBar: Material(
        elevation: 8,
        child: Container(
          color: Color(0xffE5E5E5),
          height: ScreenUtil().setHeight(80),
          child: Center(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(18),
              child: Container(
                width: ScreenUtil().setWidth(120),
                height: ScreenUtil().setHeight(50),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(18),
                ),
                child: FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () {

                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => RecordingPage(),
                      ),
                    );
                  },
                  child: Center(child: Text('Record')),
                ),
              ),
            ),
          ),
        ),
      ),
      backgroundColor: Color(0xffDFDFDF),
      appBar: AppBar(
        elevation: 6,
        title: Text(appName),
        centerTitle: true,
      ),
      body: _notesList == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.separated(
              physics: BouncingScrollPhysics(),
              separatorBuilder: (context, index) {
                return SizedBox(
                  height: ScreenUtil().setHeight(10),
                );
              },
              itemBuilder: (context, index) {
                return NoteRow(
                  notesList: _notesList.reversed.toList(),
                  index: index,
                );
              },
              itemCount: _notesCount,
            ),
    );
  }

  void _initNotes() async {
    _notesCount = await getNotesCount(_databaseHelper);
    _notesList = await getNotes(_databaseHelper);
    if (_notesList == null) {
      _notesList = [];
    }

    setState(() {});
  }
}

class NoteRow extends StatefulWidget {
  final List<Note> notesList;
  final int index;

  const NoteRow({Key key, this.notesList, this.index}) : super(key: key);

  @override
  _NoteRowState createState() => _NoteRowState();
}

class _NoteRowState extends State<NoteRow> {
  bool _clicked = false;
  DatabaseHelper _databaseHelper = DatabaseHelper();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _clicked = !_clicked;
        setState(() {});
      },
      child: Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(12),
          right: ScreenUtil().setWidth(12),
          top: ScreenUtil().setHeight(3),
        ),
        color: Color(0xffCCCCCC),
//        height: ScreenUtil().setHeight(75),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children: [
                Text(
                  widget.notesList[widget.index].date,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(10),
                ),
                Flexible(
                  child: Text(
                    widget.notesList[widget.index].content,
                    maxLines: _clicked ? 1000 : 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
            IconButton(
              icon: Icon(
                Icons.delete_forever,
              ),
              onPressed: () {
                _deleteNote(
                  widget.notesList[widget.index].id,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  void _deleteNote(int id) async {
    int result = await _databaseHelper.deleteNote(id);
    print(result);
    if (result == 1) {
      // done deleting note
      showToast(
        'Deleted',
        color: Colors.green,
      );
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => HomeScreen(),
        ),
      );
    } else {
      // error occurred
      showToast('an error occurred');
    }
  }
}

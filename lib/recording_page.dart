import 'package:avatar_glow/avatar_glow.dart';
import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'package:the_app/main.dart';

import 'app_utils.dart';
import 'database_helper.dart';
import 'note_model.dart';

class RecordingPage extends StatefulWidget {
  @override
  _RecordingpageState createState() => _RecordingpageState();
}

class _RecordingpageState extends State<RecordingPage> {
  bool _isListening = false;
  SpeechToText _speech = SpeechToText();
  String _text = 'Hold on the button to start recording';
  DatabaseHelper _databaseHelper = DatabaseHelper();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(appName),
        centerTitle: true,
      ),
      body: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: AvatarGlow(
          animate: _isListening,
          glowColor: Theme.of(context).primaryColor,
          endRadius: 75.0,
          duration: Duration(milliseconds: 2000),
          repeatPauseDuration: Duration(milliseconds: 100),
          repeat: true,
          child: GestureDetector(
            onTapDown: _listen,
            onTapUp: _saveNote,
            child: Container(
              width: ScreenUtil().setWidth(55),
              height: ScreenUtil().setHeight(55),
              decoration: BoxDecoration(
                color: Colors.red,
                shape: BoxShape.circle,
              ),
              child: Icon(
                _isListening ? Icons.mic : Icons.mic_none,
                color: Colors.white,
              ),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(18.0),
          child: Container(
            width: size.width,
            height: size.height,
            child: Stack(
              children: [
                SingleChildScrollView(
                  reverse: true,
                  child: Column(
                    children: [
                      Text(
                        _text,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 22,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _listen(details) async {
    print('holding down');
    if (!_isListening) {
      // request the permissions
      bool available = await _speech.initialize();
      // case permissions are granted
      if (available) {
        // start listening to the speech
        setState(
              () => _isListening = true,
        );
        _speech.listen(
          onResult: (val) => setState(
                () {
              _text = val.recognizedWords;
            },
          ),
        );
      }
    } else {
      // case permissions are denied
      // stop
      setState(
            () => _isListening = false,
      );
      _speech.stop();
    }
  }

  void _saveNote(details) async {
    print('holding up');
    setState(
          () => _isListening = false,
    );
    // case no diary words  =>  show toast msg
    if (_text == 'Hold on the button to start recording' || _text.isEmpty) {
      showToast('No record detected');
    } else {
      // case user make a voice dairy
      // save the dairy to database
      final dateTime = DateTime.now();
      Note theNote =
      Note(dateTime.format(AmericanDateFormats.standardWithComma), _text);
      int result = await _databaseHelper
          .addNewNote(theNote); // the returned result is the note id
      // if the result is greater than 0 so the operation is done
      if (result != 0) {
        showToast(
          'Saved',
          color: Colors.green,
        );
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => HomeScreen(),), (_) => false);
      } else {
        // an error occurred
        showToast('an error occurred');
      }
    }
  }
}

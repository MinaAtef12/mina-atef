import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'note_model.dart';

class DatabaseHelper {
  static DatabaseHelper
  _sInstance; // singleton object  (singleton means that this object will initialized only once while project is runnimg)
  static Database _database; // singleton database

  // declare column's names for easy use it in operations
  String noteTable = "noteTable";
  String colId = "id";
  String colContent = "content";
  String colDate = "date";

  // create named constructor
  DatabaseHelper._createInstance();

  factory DatabaseHelper() {
    if (_sInstance == null) {
      // if instance is null go and (create or reopen database) and get instance
      _sInstance = DatabaseHelper._createInstance();
    }

    return _sInstance;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initializeDatabase();
    }

    return _database;
  }

  Future<Database> initializeDatabase() async {
    // get the path of director that database will be stored in it for both Android and IOs
    Directory directory = await getApplicationDocumentsDirectory();
    String path = "${directory.path} notes.db";

    // open or create the database in the given path
    var theDatabase = openDatabase(path, version: 1, onCreate: createDatabase);
    return theDatabase;
  }

  void createDatabase(Database db, int version) async {
    await db.execute(
      // sql query
      "CREATE TABLE $noteTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, "
          "$colContent TEXT,"
          "$colDate TEXT);",
    );
  }

  // operations on database
  // 1- fetch all notes (will return as list of maps)
  Future<List<Map<String, dynamic>>> getNoteMapList() async {
    Database db = await this.database;
    var result =
    await db.rawQuery("SELECT * FROM $noteTable ORDER BY $colDate DESC");
    return result;
  }

  // 1.1 convert list<maps> into list<notes>
  Future<List<Note>> getAllNotes() async {
    // first get list<map>
    List<Map<String, dynamic>> list1 = await getNoteMapList();
    // create my list that will store the notes
    List<Note> list2 = List<Note>();
    for (int i = 0; i < list1.length; i++) {
      list2.add(Note.fromMapObject(list1[i]));
    }

    return list2;
  }

  // 2 inset new note
  Future<int> addNewNote(Note note) async {
    Database db = await this.database;
    var result = await db.insert("$noteTable", note.noteToMap());
    return result;
  }

  // delete note
  Future<int> deleteNote(int id) async {
    Database db = await this.database;
    var result =
    await db.rawDelete("DELETE FROM $noteTable WHERE $colId = $id");
    return result;
  }

  // get number of notes
  Future<int> getNotesCount() async {
//    Database db = await this.database;
//    List<Map<String, dynamic>> count =
//    await db.rawQuery("SELECT COUNT (*) FROM $noteTable");
//    int result = Sqflite.firstIntValue(count);

    return (await getAllNotes()).length;
  }
}
